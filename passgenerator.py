#/usr/bin/python

import argparse
import random
from random import randrange
import string

# Simple Password generator with 5 switches able to handle complex passwords.
# Made by Michael Støier
# Mail: bedja@bedjaart.com
# TODO: Implant threading to handle larger passwords above 100 chars.

# user password generator
# lists to handle count of 3 pairs and 4 pairs

newList = []
newListTwo = []

def subsetSum(numbers, target, partial=[]):
    s = sum(partial)

    # check if the partial sum is equals to target
    if s == target:
        if len(partial) == 3:
            newList.append(partial)
        elif len(partial) == 4:
            newListTwo.append(partial)

    if s >= target:
        return  # if we reach the number why bother to continue

    for i in range(len(numbers)):
        n = numbers[i]
        remaining = numbers[i+1:]
        subsetSum(remaining, target, partial + [n])

def poolGenerator(args, value):

    if value == "1":
      return args.length
    elif value == "2":
        pool = args.length
        poolPickOne = randrange(1, pool)
        poolPickTwo = pool - poolPickOne
        return poolPickTwo, poolPickOne

    elif value == "3":
        subsetSum(range(1, args.length), args.length)
        return newList
    elif value == "4":
        subsetSum(range(1, args.length), args.length)
        return newListTwo

def stringConvert(myString):
    li = list(myString.split(" "))
    return li

def listToSting(myString):
    s = ""
    for e in myString:
        s += e
    return s

def generatorTwoItems(args, dataOne, dataTwo):
        dataOne = argsConverter(args, dataOne)
        dataTwo = argsConverter(args, dataTwo)
        pool = poolGenerator(args, "2")
        dataOne = str(' '.join(random.choice(dataOne) for i in range(int(pool[0]))))
        dataTwo = str(' '.join(random.choice(dataTwo) for i in range(int(pool[1]))))
        data = dataOne + dataTwo
        data = stringConvert(data)
        dataClone = data
        random.shuffle(dataClone)
        print(f'Random generated password: {listToSting(dataClone)}')

def generatorFourItems(args, dataOne, dataTwo, dataThree, dataFour):
    dataOne = argsConverter(args, dataOne)
    dataTwo = argsConverter(args, dataTwo)
    dataThree = argsConverter(args, dataThree)
    dataFour = argsConverter(args, dataFour)
    poolGenerator(args, "4")
    if newListTwo == []:
        backupList = [2,2,2,2]
        newListTwo.append(backupList)

    data = random.choice(newListTwo)
    dataOne = str(' '.join(random.choice(dataOne) for i in range(int(data[0]))))
    dataTwo = str(' '.join(random.choice(dataTwo) for i in range(int(data[1]))))
    dataThree = str(' '.join(random.choice(dataThree) for i in range(int(data[2]))))
    dataFour = str(' '. join(random.choice(dataFour) for i in range(int(data[3]))))
    data = dataOne + dataTwo + dataThree + dataFour
    data = stringConvert(data)
    dataClone = data
    random.shuffle(dataClone)
    print(f'Random generated password: {listToSting(dataClone)}')


def generatorThreeItems(args, dataOne, dataTwo, dataThree):
    dataOne = argsConverter(args, dataOne)
    dataTwo = argsConverter(args, dataTwo)
    dataThree = argsConverter(args, dataThree)
    poolGenerator(args, "3")
    if newList == []:
        backupList = [2,2,2]
        newList.append(backupList)
    data = random.choice(newList)
    dataOne = str(' '.join(random.choice(dataOne) for i in range(int(data[0]))))
    dataTwo = str(' '.join(random.choice(dataTwo) for i in range(int(data[1]))))
    dataThree = str(' '.join(random.choice(dataThree) for i in range(int(data[2]))))
    data = dataOne + dataTwo + dataThree
    data = stringConvert(data)
    dataClone = data
    random.shuffle(dataClone)
    print(f'Random generated password: {listToSting(dataClone)}')

def argsConverter(args, conversionType):
    data = None
    if conversionType == "lower":
        data = string.ascii_lowercase
    elif conversionType == "upper":
        data = string.ascii_uppercase
    elif conversionType == "digits":
        data = string.digits
    elif conversionType == "special":
        data = string.punctuation
    return str(''.join(random.choice(data) for i in range(args.length)))

def generator(args):

    # 4 active switches:
    if args.specialChar is True and args.lowerCase is True and args.upperCase is True and args.digits is True:
        generatorFourItems(args, "special", "lower", "upper", "digits")

    # 3 active switches
    # s,l,u
    elif args.specialChar is True and args.lowerCase is True and args.upperCase is True:
        generatorThreeItems(args, "special", "lower", "upper")
    # d,u,s
    elif args.digits is True and args.upperCase is True and args.specialChar is True:
        generatorThreeItems(args, "digits", "upper", "special")
    # l,d,s
    elif args.lowerCase is True and args.digits is True and args.specialChar is True:
        generatorThreeItems(args, "lower", "digits", "special")
    # l,d,u
    elif args.lowerCase is True and args.digits is True and args.upperCase is True:
        generatorThreeItems(args, "lower", "digits", "upper")
    # l,d,u,s

    # u,s
    elif args.upperCase is True and args.specialChar is True:
        generatorTwoItems(args, "upper", "special")

    # l,u , l,s
    elif args.lowerCase is True and args.upperCase is True:
        generatorTwoItems(args, "lower", "upper")
    elif args.lowerCase is True and args.specialChar is True:
        generatorTwoItems(args, "lower", "special")

    # d,l , d,u , d,s
    elif args.digits is True and args.lowerCase is True:
        generatorTwoItems(args, "digits", "lower")
    elif args.digits is True and args.upperCase is True:
        generatorTwoItems(args, "digits", "upper")
    elif args.digits is True and args.specialChar is True:
        generatorTwoItems(args, "digits", "special")

    # The four base values / 1 switch:
    elif args.lowerCase is True:
        data = argsConverter(args, "lower")
        print(f'Random generated password: {data}')

    elif args.digits is True:
        data = argsConverter(args, "digits")
        print(f'Random generated password: {data}')

    elif args.specialChar is True:
        data = argsConverter(args, "special")
        print(f'Random generated password: {data}')

    elif args.upperCase is True:
        data = argsConverter(args, "upper")
        print(f'Random generated password: {data}')

    else:
        # Revert to default 8 char lowercase
        data = argsConverter(args, "lower")
        print(f'Random generated password: {data}')
        #Test data for value 3
        #data = poolGenerator(args, "3")
        #print(data)

def main():
    parser = argparse.ArgumentParser(description='Usage of program:' + '-L <length> -d <digits> -u <uppercase> -s <specialchar> -l <lowercase>')
    parser.add_argument('-L', dest='length', default=8, type=int, help='specify the length / default 8')
    parser.add_argument('-d', dest='digits', action='store_true', help='specify if you like digits')
    parser.add_argument('-u', dest='upperCase', action='store_true', help='specify if you like uppercase')
    parser.add_argument('-s', dest='specialChar', action='store_true', help='specify if you like special characters')
    parser.add_argument('-l', dest='lowerCase', action='store_true', help='specify if you like lowercase')
    args = parser.parse_args()
    generator(args)

if __name__ == '__main__':
    main()

